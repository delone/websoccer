package volley.mobilesoccer;

import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import volley.mobilesoccer.adapters.CompetitionAdapter;
import volley.mobilesoccer.adapters.PlayerAdapter;
import volley.mobilesoccer.fragments.CompetitionsFragment;
import volley.mobilesoccer.models.Competition;
import volley.mobilesoccer.models.Player;
import volley.mobilesoccer.models.Team;

public class PlayerActivity extends AppCompatActivity {

    private final String TAG = CompetitionsFragment.class.getName();
    PlayerAdapter adapter;
    RecyclerView recyclerView;
    List<Player> list;
    ImageView loading10;
    AnimationDrawable animationDrawable;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player);
        getSupportActionBar().setTitle("Players");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        loading10 = (ImageView)this.findViewById(R.id.loading10);
        animationDrawable = (AnimationDrawable)loading10.getBackground();
        recyclerView = (RecyclerView) this.findViewById(R.id.player_recycler_view);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        list = new ArrayList<>();
        adapter = new PlayerAdapter(list,this);
        recyclerView.setAdapter(adapter);
        animationDrawable.start();
        Intent intent = getIntent();

        String tag_json_arry = "json_array_req";

        String url = intent.getStringExtra("linkPlayer");
Log.v("MY URL IS _ " , url);
         JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                JSONArray players = null;
                try {
                    players = response.getJSONArray("players");
                    for (int i = 0; i < players.length(); i++) {
                        JSONObject item = players.getJSONObject(i);
                        String name = item.getString("name") ;
                        String position = item.getString("position") ;
                        String jerseyNumber = item.getString("jerseyNumber") ;
                        String dateOfBirth = item.getString("dateOfBirth");
                        String nationality = item.getString("nationality") ;
                        Player player = new Player(name, position, jerseyNumber, dateOfBirth, nationality);
                        list.add(player);
                    }

                    adapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                animationDrawable.stop();
                loading10.setVisibility(View.INVISIBLE);

            }
            }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.v("Erorr" ,error.getMessage());
                animationDrawable.stop();
                loading10.setVisibility(View.INVISIBLE);
            }
            }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                headers.put("X-Auth-Token", "7175220d226744269e5fee998421e70d");
                return headers;
            }
            };

        MyApplication.getInstance().addToRequestQueue(jsonObjectRequest, tag_json_arry);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() ==  android.R.id.home) {

            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}

