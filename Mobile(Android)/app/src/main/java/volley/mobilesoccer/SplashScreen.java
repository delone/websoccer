package volley.mobilesoccer;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;

/**
 * Created by Administrator on 06.11.2016.
 */
public class SplashScreen extends AppCompatActivity {

    CountDownTimer tim;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
/*        getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
        getActionBar().hide();*/
        setContentView(R.layout.splash_screen);

        getPreference();
    }

    private void getPreference() {

            tim = new CountDownTimer(3000, 1000) {

                public void onTick(long millisUntilFinished) {

                }

                public void onFinish() {

                    Intent intent = new Intent(SplashScreen.this, MainActivity.class);
                    startActivity(intent);
                    System.gc();
                    //finish();
                }
            }
                    .start();

        }

    @Override
    public void onBackPressed() {
        tim.cancel();
        finish();
        //  return;
        //   super.onBackPressed();
        // finish();
    }

    @Override
    protected void onPause() {
        super.onPause();
        finish();
    }


    }

