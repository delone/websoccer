package volley.mobilesoccer;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import volley.mobilesoccer.adapters.CompetitionRectAdapter;
import volley.mobilesoccer.adapters.TeamAdapter;
import volley.mobilesoccer.models.Competition;
import volley.mobilesoccer.models.Team;

public class TeamActivity extends AppCompatActivity {

    ImageView loading10;
    AnimationDrawable animationDrawable;
    GridView gridView1;
    ArrayList<Team> list;
    TeamAdapter adapter;
    Context con ;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_team);

        //getSupportActionBar().setTitle("Teams");
        loading10 = (ImageView)this.findViewById(R.id.loading10);
        gridView1= (GridView)this.findViewById(R.id.gridView1);
        animationDrawable = (AnimationDrawable)loading10.getBackground();

        String tag_json_arry = "json_array_req";
        list = new ArrayList<>();
        adapter = new TeamAdapter(this,list);
        gridView1.setAdapter(adapter);
        int league_id  = 0;
        final Intent intent = getIntent();
        if (intent != null){
            league_id = intent.getIntExtra("id",0);
        }
        con = this;



        String url = "http://api.football-data.org/v1/competitions/"+league_id+"/teams";
        animationDrawable.start();

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

            JSONArray teams = null;
                try {
                    teams = response.getJSONArray("teams");

                    for (int j = 0; j < teams.length(); j++) {

                        JSONObject item = null;

                        item = teams.getJSONObject(j);


                        String linkPlayer = "";
                        JSONObject item1 = null;
                        try {
                            item1 = item.getJSONObject("_links");
                            if(item1 != null) {
                                JSONObject item2 = item1.getJSONObject("players");
                                linkPlayer = item2.getString("href");
                            }
                        } catch (JSONException e1) {
                            e1.printStackTrace();
                        }

                        String name = item.getString("name");
                        String shortname = item.getString("shortName");
                        String squadMarketValue = item.getString("squadMarketValue");
                        String crestUrl   ="http://7606-presscdn-0-74.pagely.netdna-cdn.com/wp-content/uploads/2016/03/Dubai-P" +
                                "hotos-Images-Oicture-Dubai-Landmarks-800x600.jpg";

                        Team team = new Team(linkPlayer, name, shortname, squadMarketValue, crestUrl);
                        list.add(team);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }



                adapter.notifyDataSetChanged();
                gridView1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Team team = list.get(position);
                        Intent intent1 = new Intent(con,PlayerActivity.class);
                        intent1.putExtra("linkPlayer",team.getLinkPlayer());
                        startActivity(intent1);
                    }
                });

                animationDrawable.stop();
                loading10.setVisibility(View.INVISIBLE);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
Log.v("Erorr" ,error.getMessage());
                animationDrawable.stop();
                loading10.setVisibility(View.INVISIBLE);
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                headers.put("X-Auth-Token", "7175220d226744269e5fee998421e70d");
                return headers;
            }
        };


        MyApplication.getInstance().addToRequestQueue(jsonObjectRequest, tag_json_arry);


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() ==  android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
