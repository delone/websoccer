package volley.mobilesoccer.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import volley.mobilesoccer.AboutNews;
import volley.mobilesoccer.R;
import volley.mobilesoccer.utils.RoundedTransformation;
import volley.mobilesoccer.models.News;

/**
 * Created by Eldar on 11/5/2016.
 */
public class Adapter  extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    public final int VIEW_TYPE_ITEM_ONE_COLUMN = 1;
    public final int VIEW_TYPE_ITEM_TWO_COLUMN = 2;
    public final int VIEW_TYPE_LOADING = 0;

    Context mContext;
    ArrayList<News> list;

    public Adapter( Context mContext, ArrayList list) {
        this.mContext = mContext;
        this.list = list;
    }

    @Override
    public int getItemViewType(int position) {
        return list.get(position) == null ? VIEW_TYPE_LOADING : (position+1) % 5 == 1 ?
                VIEW_TYPE_ITEM_ONE_COLUMN : VIEW_TYPE_ITEM_TWO_COLUMN;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM_ONE_COLUMN){
            View view = LayoutInflater.from(mContext).inflate(R.layout.item_list_one_column, parent, false);
            return new ItemViewHolder(view);
        }else if(viewType == VIEW_TYPE_ITEM_TWO_COLUMN){
            View view = LayoutInflater.from(mContext).inflate(R.layout.item_list, parent, false);
            return new ItemViewHolder(view);
        }else if(viewType == VIEW_TYPE_LOADING){
            View view = LayoutInflater.from(mContext).inflate(R.layout.layout_loading_item, parent, false);
            return new LoadingViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof ItemViewHolder){
            final ItemViewHolder itemViewHolder = (ItemViewHolder) holder;

            itemViewHolder.txtTitle.setText(list.get(position).getTitle());
            itemViewHolder.txtDate.setText(list.get(position).getDate());

            if (!list.get(position).getPhotoUrl().isEmpty()){
                Picasso picasso = Picasso.with(mContext);
                picasso.load(list.get(position).getPhotoUrl()).transform(new RoundedTransformation(20, 0)).into(itemViewHolder.imgSource,
                        new Callback() {
                            @Override
                            public void onSuccess() {
                                itemViewHolder.imgLoading.setVisibility(View.INVISIBLE);
                            }

                            @Override
                            public void onError() {
                            }
                        });
            }

            itemViewHolder.imgSource.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(mContext, AboutNews.class);
                    News news = list.get(position);
                    int id =news.getId();
                    intent.putExtra("Newid", id);
                    intent.putExtra("date", news.getDate());
                    mContext.startActivity(intent);
                }
            });
        }else if( holder instanceof LoadingViewHolder){
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }
    }

    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    static class ItemViewHolder extends RecyclerView.ViewHolder{
        ImageView imgSource;
        ImageView imgLoading;
        TextView txtTitle;
        TextView  txtDate;
        TextView  txtViewCount;

        public ItemViewHolder(View itemView) {
            super(itemView);
            imgSource = (ImageView) itemView.findViewById(R.id.imgSource);
            imgLoading = (ImageView) itemView.findViewById(R.id.imgLoding);
            txtTitle = (TextView) itemView.findViewById(R.id.txtTitle);
            txtDate = (TextView) itemView.findViewById(R.id.txtDate);
            txtViewCount = (TextView) itemView.findViewById(R.id.txtViews);
        }
    }

    static class LoadingViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public LoadingViewHolder(View itemView) {
            super(itemView);
            progressBar = (ProgressBar) itemView.findViewById(R.id.progressBar1);
        }
    }
}