package volley.mobilesoccer.adapters;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableLayout;
import android.widget.TextView;

import java.util.HashMap;
import java.util.List;

import volley.mobilesoccer.R;
import volley.mobilesoccer.models.CupTable;

/**
 * Created by Administrator on 05.11.2016.
 */
public class CupTableAdapter extends RecyclerView.Adapter<CupTableAdapter.MyViewHolder> {

    private HashMap<String,List<CupTable>> hashMap;
    private List<CupTable> list;
    private List<String> groups;
    Context mContext;

    public CupTableAdapter(List<CupTable> list,List<String> groups, Context mContext) {
        this.list = list;
        this.groups = groups;
        this.mContext = mContext;

    }

    @Override
    public CupTableAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mContext)
                .inflate(R.layout.cup_list_row, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CupTableAdapter.MyViewHolder holder, int position) {


        holder.league_name1.setText(list.get(position*3+position).getTeam());
        holder.goals1.setText(list.get(position*3+position).getGoals()+":"+list.get(position).getGoalAgainst());
        holder.points1.setText(list.get(position*3+position).getPoints());

        holder.league_name2.setText(list.get(position*3+position+1).getTeam());
        holder.goals2.setText(list.get(position*3+position+1).getGoals()+":"+list.get(position+1).getGoalAgainst());
        holder.points2.setText(list.get(position*3+position+1).getPoints());

        holder.league_name3.setText(list.get(position*3+position+2).getTeam());
        holder.goals3.setText(list.get(position*3+position+2).getGoals()+":"+list.get(position+1).getGoalAgainst());
        holder.points3.setText(list.get(position*3+position+2).getPoints());

        holder.league_name4.setText(list.get(position*3+position+3).getTeam());
        holder.goals4.setText(list.get(position*3+position+3).getGoals()+":"+list.get(position+1).getGoalAgainst());
        holder.points4.setText(list.get(position*3+position+3).getPoints());

        holder.group_name.setText("Group "+groups.get(position));

    }

    @Override
    public int getItemCount() {
        return list.size()/4;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TableLayout tableLayout;

        TextView order1, league_name1, goals1, points1;
        TextView order2, league_name2, goals2, points2;
        TextView order3, league_name3, goals3, points3;
        TextView order4, league_name4, goals4, points4;

        TextView group_name;


        public MyViewHolder(View itemView) {
            super(itemView);
            tableLayout = (TableLayout) itemView.findViewById(R.id.table_layout);
            order1 = (TextView) itemView.findViewById(R.id.order1);
            order1.setTextColor(ColorStateList.valueOf(Color.BLACK));
            league_name1 = (TextView) itemView.findViewById(R.id.league_name1);
            league_name1.setTextColor(ColorStateList.valueOf(Color.BLACK));
            goals1 = (TextView) itemView.findViewById(R.id.goals1);
            points1 = (TextView) itemView.findViewById(R.id.points1);

            order2 = (TextView) itemView.findViewById(R.id.order2);
            league_name2 = (TextView) itemView.findViewById(R.id.league_name2);
            goals2 = (TextView) itemView.findViewById(R.id.goals2);
            points2 = (TextView) itemView.findViewById(R.id.points2);

            order3 = (TextView) itemView.findViewById(R.id.order3);
            league_name3 = (TextView) itemView.findViewById(R.id.league_name3);
            goals3 = (TextView) itemView.findViewById(R.id.goals3);
            points3 = (TextView) itemView.findViewById(R.id.points3);

            order4 = (TextView) itemView.findViewById(R.id.order4);
            league_name4 = (TextView) itemView.findViewById(R.id.league_name4);
            goals4 = (TextView) itemView.findViewById(R.id.goals4);
            points4 = (TextView) itemView.findViewById(R.id.points4);

            group_name = (TextView) itemView.findViewById(R.id.group_name);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            System.out.println("position: "+ getPosition());

        }
    }
}
