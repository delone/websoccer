package volley.mobilesoccer.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;


import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import volley.mobilesoccer.MyApplication;
import volley.mobilesoccer.R;
import volley.mobilesoccer.utils.Storage;

/**
 * Created by Administrator on 06.10.2016.
 */
public class FavoritesAdapter extends BaseAdapter {

    private List<String> categorylist;
    private List<Integer> categoryIds;
    private List<Integer> list;

    private LayoutInflater inflater;
    CheckBox checkBox;
    Context context;
    Storage storage;

    AsyncHttpClient client;

    public FavoritesAdapter(List<String> categorylist, List<Integer> categoryIds, List<Integer> list, Context context) {
        this.list = list;
        this.categorylist = categorylist;
        this.categoryIds = categoryIds;
        this.context = context;
        inflater =(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return categorylist.size();
    }

    @Override
    public Object getItem(int i) {
        return categoryIds.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int position, View view, ViewGroup viewGroup) {

        view = inflater.inflate(R.layout.favorites_one_row,viewGroup, false);
        final TextView title = (TextView) view.findViewById(R.id.tvtenzimleme);
        checkBox = (CheckBox) view.findViewById(R.id.checkbox);
        storage = Storage.getInstance(context);
        client = new AsyncHttpClient();

        title.setText(categorylist.get(position));
        checkBox.setChecked(list.contains(categoryIds.get(position)));

        checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final CheckBox checkbox = (CheckBox) view;
                System.out.println("CHeckbox: "+checkbox.isChecked());
                System.out.println("user id:"+storage.getUserId());
                String url ="http://websport.azurewebsites.net/Mobile/AddtoFavorites";
                JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, null, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {


                            String flag = response.toString();
                           // notifyDataSetChanged();

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        System.out.println("error");
                    }
                }){
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("CategoryId", categoryIds.get(position).toString());
                        params.put("AppId", storage.getUserId());
                        params.put("SetAction", checkbox.isChecked() ? "1" : "2");
                        return params;
                    }
                };

                // Adding request to request queue
                MyApplication.getInstance().addToRequestQueue(request, "test");
            }

        });


        return view;
    }

    // check if network available
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}


