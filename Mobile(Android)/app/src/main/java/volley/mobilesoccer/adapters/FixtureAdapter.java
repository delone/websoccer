package volley.mobilesoccer.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import volley.mobilesoccer.R;
import volley.mobilesoccer.models.Fixture;

/**
 * Created by Administrator on 06.11.2016.
 */
public class FixtureAdapter  extends RecyclerView.Adapter<FixtureAdapter.MyViewHolder> {

    List<Fixture> list;
    Context mContext;

    public FixtureAdapter(List<Fixture> list, Context mContext) {
        this.list = list;
        this.mContext = mContext;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mContext)
                .inflate(R.layout.fixture_list_row, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Fixture fixture = list.get(position);
        holder.week.setText(fixture.getMatchday());
        holder.date.setText(fixture.getDate());
        holder.home.setText(fixture.getHomeTeamName());
        holder.away.setText(fixture.getAwayTeamName());

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView week, date, home, away;

        public MyViewHolder(View itemView) {
            super(itemView);
            week = (TextView) itemView.findViewById(R.id.week);
            date = (TextView) itemView.findViewById(R.id.date);
            home = (TextView) itemView.findViewById(R.id.home);
            away = (TextView) itemView.findViewById(R.id.away);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            System.out.println("position: " + getPosition());



        }
    }
}
