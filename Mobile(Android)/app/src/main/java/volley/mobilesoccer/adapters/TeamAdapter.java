package volley.mobilesoccer.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import volley.mobilesoccer.R;
import volley.mobilesoccer.utils.RoundedTransformation;
import volley.mobilesoccer.models.Team;

/**
 * Created by Eldar on 11/5/2016.
 */

public class TeamAdapter extends BaseAdapter {


    Context context;
    ArrayList<Team> list;

    ImageView imgSoource;
    TextView title;
    LayoutInflater vi;

    public TeamAdapter(Context context, ArrayList<Team> list){
        this.context=context;
        this.list=list;
        vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;
        if (v==null)
            v = vi.inflate(R.layout.single_item, null);

        imgSoource = (ImageView)v.findViewById(R.id.imgSource9);
        imgSoource.setTag(position);
        title = (TextView) v.findViewById(R.id.title);
        title.setText(list.get(position).getName());
        imgSoource.setImageResource(R.drawable.ball);
        return v;
    }

}
