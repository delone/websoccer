package volley.mobilesoccer.adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Random;

import volley.mobilesoccer.R;
import volley.mobilesoccer.models.NewsAbout;

public class ViewPagerAdapter extends PagerAdapter {

    ImageView news_bckgrnd;
    Context context;
    ArrayList<NewsAbout> list;

    ImageView imgurl;
    ImageView imgPlay, imgSave;


    int image;

    LayoutInflater inflater;

    public ViewPagerAdapter(Context context, ArrayList<NewsAbout> list) {
        this.list = list;
        this.context = context;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public Parcelable saveState() {
        return super.saveState();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((FrameLayout) object);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager) container).removeView((FrameLayout) object);
    }

    @Override
    public Object instantiateItem(final ViewGroup container, final int position) {

        View itemView = inflater.inflate(R.layout.viewpagerimage, container, false);

        imgurl = (ImageView) itemView.findViewById(R.id.imgUrl);
        imgPlay = (ImageView) itemView.findViewById(R.id.imgPlay);
        imgSave = (ImageView) itemView.findViewById(R.id.saveimage);
        news_bckgrnd = (ImageView) itemView.findViewById(R.id.news_bckgrnd);




            Picasso picasso = Picasso.with(context);
            picasso.load(list.get(position).getPhoto_url()).into(imgurl, new Callback() {
                @Override
                public void onSuccess() {
                    news_bckgrnd.setVisibility(View.INVISIBLE);


                }

                @Override
                public void onError() {

                }
            });



        ((ViewPager) container).addView(itemView);

        return itemView;
    }
    public String md5(String s) {
        try {
            // Create MD5 Hash
            MessageDigest digest = java.security.MessageDigest.getInstance("MD5");
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuffer hexString = new StringBuffer();
            for (int i=0; i<messageDigest.length; i++)
                hexString.append(Integer.toHexString(0xFF & messageDigest[i]));
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    public Bitmap getBitmapFromURL(String imageUrl) {
        try {
            URL url = new URL(imageUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private String saveBitmap(Bitmap finalBitmap, String name) {
        String urls = "empty";
        String root = Environment.getExternalStorageDirectory().toString();
        File myDir = new File(root + "/goal.az");
        myDir.mkdirs();
        Random generator = new Random();
        int n = 10000;
        n = generator.nextInt(n);
       // String fname = System.currentTimeMillis() + ".jpg";
        String fname = name + ".jpg";
        File file = new File(myDir, fname);
        if (file.exists()) file.delete();
        try {
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
            out.flush();
            out.close();


        } catch (Exception e) {
            e.printStackTrace();
        }
        String root2 = Environment.getExternalStorageDirectory().toString();
        urls = root2 + "/goal.az/" + fname;
        return urls;
    }

    @Override
    public int getCount() {
        return list.size();
    }
}
