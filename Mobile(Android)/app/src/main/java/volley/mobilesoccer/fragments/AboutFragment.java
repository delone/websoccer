package volley.mobilesoccer.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;

import volley.mobilesoccer.MainActivity;
import volley.mobilesoccer.R;

/**
 * Created by Administrator on 06.11.2016.
 */
public class AboutFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.about, container, false);

        WebView webview = (WebView) view.findViewById(R.id.web);
        webview.getSettings().setJavaScriptEnabled(true);

        ((MainActivity) getActivity()).setActionBarTitle("About");

        WebSettings settings = webview.getSettings();
        settings.setDefaultTextEncodingName("utf-8");
        webview.loadUrl("file:///android_asset/file.html");


        return view;
    }
}
