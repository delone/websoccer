package volley.mobilesoccer.fragments;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import volley.mobilesoccer.MyApplication;
import volley.mobilesoccer.R;

/**
 * Created by Administrator on 06.11.2016.
 */
public class CategoryDialogFragment extends Dialog implements AdapterView.OnItemSelectedListener, View.OnClickListener {

    List<String> categoryTitles;
    List<Integer> categoryIds;
    ArrayAdapter<String>  dataAdapter;
    Spinner spinner;

    Button button;
    Integer category_id;

    OnMyDialogResult mDialogResult;

    public CategoryDialogFragment(Context context) {
        super(context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.category_dialog);


        button = (Button) findViewById(R.id.search);
        spinner = (Spinner) findViewById(R.id.spinner);

        // Spinner click listener
        spinner.setOnItemSelectedListener(this);

        // Spinner Drop down elements
        categoryIds = new ArrayList<>();
        categoryTitles = new ArrayList<>();



        String url = "http://websport.azurewebsites.net/Mobile/GetCategories";
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                JSONArray categories = null;
                try {
                    categories = response.getJSONArray("categoryList");
                    if (categories.length()>0){
                        for (int i=0; i<categories.length();i++){
                            JSONObject item = categories.getJSONObject(i);
                            categoryTitles.add(item.getString("Name"));
                            categoryIds.add(item.getInt("Id"));

                            // Creating adapter for spinner
                            dataAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, categoryTitles);

                            // Drop down layout style - list view with radio button
                            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                            // attaching data adapter to spinner
                            spinner.setAdapter(dataAdapter);
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });


        MyApplication.getInstance().addToRequestQueue(request, "Category Dialog");

        button.setOnClickListener(this);

    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        String item = adapterView.getItemAtPosition(i).toString();

            category_id = categoryIds.get(i);
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    @Override
    public void onClick(View view) {
        mDialogResult = (OnMyDialogResult) getOwnerActivity();
        if( mDialogResult != null ){
            mDialogResult.finish(String.valueOf(category_id));
        }

        CategoryDialogFragment.this.dismiss();
    }

    public void setDialogResult(OnMyDialogResult dialogResult){
        mDialogResult = dialogResult;
    }

    public interface OnMyDialogResult{
        void finish(String result);
    }
}
