package volley.mobilesoccer.fragments;

import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;


import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.loopj.android.http.AsyncHttpClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import volley.mobilesoccer.MainActivity;
import volley.mobilesoccer.MyApplication;
import volley.mobilesoccer.R;
import volley.mobilesoccer.adapters.Adapter;
import volley.mobilesoccer.models.News;

import static volley.mobilesoccer.MyApplication.TAG;


public class NewsFragment extends Fragment implements CategoryDialogFragment.OnMyDialogResult {
    AsyncHttpClient client;
    int current_id = 1;
    News news;

    boolean flag = false;

    ImageView loading;
    AnimationDrawable animationDrawable;

    private RecyclerView recyclerView;

    private Adapter adapter;
    private ArrayList<News> list = new ArrayList<>();

    private boolean isLoading;
    private int visibleThreshold = 2;
    private int lastVisibleItem, totalItemCount;

    String id;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.news_fragment_layout, container, false);
        ((MainActivity) getActivity()).setActionBarTitle("The News");



        recyclerView = (RecyclerView) view.findViewById(R.id.recycleView);
        loading = (ImageView) view.findViewById(R.id.loading);

        adapter = new Adapter(this.getContext(),list);
        recyclerView.setAdapter(adapter);
        animationDrawable = (AnimationDrawable) loading.getBackground();

        final GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 2, GridLayoutManager.VERTICAL, false);
        gridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                switch(adapter.getItemViewType(position)) {
                    case 1:
                        return 2;
                    case 2:
                        return 1;
                    default:
                        return 2;
                }
            }
        });
        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                totalItemCount = gridLayoutManager.getItemCount();
                lastVisibleItem = gridLayoutManager.findLastVisibleItemPosition();

                if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                    current_id++;
                    getNews();
                    isLoading = true;
                }
            }
        });
           getNews();
        return view;
    }

    private void getNews() {

        String url = "http://websport.azurewebsites.net/Mobile/GetAllNews?Page=" + current_id + "&categoryId=-1";
        String tag_json_arry = "json_array_req";

        if (!flag){
            loading.setVisibility(View.VISIBLE);
            animationDrawable.start();

        }else{
            list.add(null);
            adapter.notifyItemInserted(list.size() - 1);
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                if (!flag){
                    animationDrawable.stop();
                    loading.setVisibility(View.INVISIBLE);
                }else{
                    list.remove(list.size()-1);
                    adapter.notifyItemRemoved(list.size());
                }

                try {
                    JSONArray arr = response.getJSONArray("newsList");

                    for (int i = 0; i < arr.length(); i++) {
                        JSONObject item = arr.getJSONObject(i);
                        Integer id = item.getInt("Id");
                        String title = item.getString("Title");
                        String content = item.getString("Content");
                        String photoUrl = item.getString("PhotoUrl");
                        String viewCount = item.getString("ViewCount");
                        String publishDate = item.getString("PublishDate");
                        if(publishDate != "null"){
                            publishDate = publishDate.replace("/Date(", "").replace(")/","");
                        }
                        Date d = new Date(Long.valueOf(publishDate));
                        DateFormat df = new SimpleDateFormat("dd.MM.yy");
                        News news = new News(content, id, title, df.format(d).toString(), viewCount, photoUrl);
                        list.add(news);
                        flag = true;
                        isLoading = false;
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                adapter.notifyDataSetChanged();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                if (isNetworkAvailable()){

                }else {
                    Toast.makeText(getActivity(), R.string.new_toast, Toast.LENGTH_SHORT).show();
                }

                loading.setVisibility(View.INVISIBLE);
                animationDrawable.stop();
                VolleyLog.d(TAG, "Error: " + error.getMessage());

            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                headers.put("X-Auth-Token", "7175220d226744269e5fee998421e70d");
                return headers;
            }
        };
        MyApplication.getInstance().addToRequestQueue(jsonObjectRequest, tag_json_arry);
    }
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    @Override
    public void finish(String result) {
        id=result;
    }
}