package volley.mobilesoccer.models;

/**
 * Created by Administrator on 05.11.2016.
 */
public class CupTable {

    private String group,team,points,goals,goalAgainst, image;

    public CupTable(String group, String team, String points, String goals, String goalAgainst, String image) {
        this.group = group;
        this.team = team;
        this.points = points;
        this.goals = goals;
        this.goalAgainst = goalAgainst;
        this.image = image;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getTeam() {
        return team;
    }

    public void setTeam(String team) {
        this.team = team;
    }

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }

    public String getGoals() {
        return goals;
    }

    public void setGoals(String goals) {
        this.goals = goals;
    }

    public String getGoalAgainst() {
        return goalAgainst;
    }

    public void setGoalAgainst(String goalAgainst) {
        this.goalAgainst = goalAgainst;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
