package volley.mobilesoccer.models;

/**
 * Created by User-PC on 3/27/2015.
 */
public class Images {

    Integer id;
    String caption;
    String league;
    String numberOfTeams;
    String photo_url;

    public Images(Integer id, String caption, String league, String photo_url, String numberOfTeams){
        this.id=id;
        this.caption=caption;
        this.league=league;
        this.photo_url = photo_url;
        this.numberOfTeams = numberOfTeams;
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public String getLeague() {
        return league;
    }

    public void setLeague(String league) {
        this.league = league;
    }

    public String getNumberOfTeams() {
        return numberOfTeams;
    }

    public void setNumberOfTeams(String numberOfTeams) {
        this.numberOfTeams = numberOfTeams;
    }

    public String getPhoto_url() {
        return photo_url;
    }

    public void setPhoto_url(String photo_url) {
        this.photo_url = photo_url;
    }


}
