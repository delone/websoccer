package volley.mobilesoccer.models;

/**
 * Created by Eldar on 11/5/2016.
 */

public class News {

    String content;
    Integer id;
    String title;
    String date;
    String photoUrl;

    public News(String content, Integer id, String title, String date, String views, String photoUrl) {
        this.content = content;
        this.id = id;
        this.title = title;
        this.date = date;
        this.photoUrl = photoUrl;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String melumat_novu) {
        this.content = content;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }



}

