package volley.mobilesoccer.models;

public class NewsAbout {

    Integer id;
    String title;
    String content;
    String photo_url;
    String publishDate;

    public String getPublishDate() {
        return publishDate;
    }

    public void setPublishDate(String publishDate) {
        this.publishDate = publishDate;
    }

    public NewsAbout(String title, String content, String photo_url, String publishDate) {

        this.title = title;
        this.content = content;
        this.photo_url = photo_url;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getPhoto_url() {
        return photo_url;
    }

    public void setPhoto_url(String video_id) {
        this.photo_url = photo_url;
    }

}
