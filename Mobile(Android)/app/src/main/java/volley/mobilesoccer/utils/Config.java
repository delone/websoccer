package volley.mobilesoccer.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Administrator on 10.09.2016.
 */
public class Config {



    public static String convertEpochToDate(String epoch){
        Date date = new Date(Long.parseLong(epoch)*1000);

        DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm");

        return formatter.format(date);
    }



}
