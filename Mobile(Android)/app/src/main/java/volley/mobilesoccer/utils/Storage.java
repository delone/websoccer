package volley.mobilesoccer.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Administrator on 06.10.2016.
 */
public class Storage {

    private static Storage mStorage;
    private SharedPreferences sharedPreferences;

    public static Storage getInstance(Context context){
        if (mStorage == null)
            mStorage = new Storage(context);

        return mStorage;
    }

    private Storage(Context context){
        sharedPreferences = context.getSharedPreferences("Storage", Context.MODE_PRIVATE);
    }

    public void setUserId(String userid){
    SharedPreferences.Editor prefsEditor = sharedPreferences.edit();
        prefsEditor.putString("userid", userid);
        prefsEditor.commit();
    }

    public String getUserId(){
        if (sharedPreferences != null)
            return sharedPreferences.getString("userid","");
        return "";
    }
}
