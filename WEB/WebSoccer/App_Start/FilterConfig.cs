﻿using System.Web;
using System.Web.Mvc;
using WebSoccer.BusinessLayer.Filters;

namespace WebSoccer
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            filters.Add(new AuthenticateAdmin());
        }
    }
}
