﻿using System;
using System.Configuration;
using System.IO;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;

namespace WebSoccer.BusinessLayer.FileUpload
{
    public class BlobUtility
    {
        //static readonly string UserConnectionString = string.Format("DefaultEndpointsProtocol=https;AccountName=mobsoccer.blob.core.windows.net;AccountKey=e6d1d191-5b8f-4d1b-a76a-31fa82116335");

        readonly CloudStorageAccount _storageAccount = CloudStorageAccount.Parse("DefaultEndpointsProtocol=https;AccountName=mobsoccer;AccountKey=F63Awga4vwl3Gz2AVA72uTRS1I6oQaONSoOEzHwlod6vIWJYCLxPefKWl/jSXwTkBKyN9r2vIy/y5Pc5cggeXw==");

        public CloudBlockBlob UploadBlob(string blobName, string containerName, Stream stream)
        {

            CloudBlobClient blobClient = _storageAccount.CreateCloudBlobClient();
            CloudBlobContainer container = blobClient.GetContainerReference(containerName.ToLower());
            container.CreateIfNotExists();
            CloudBlockBlob blockBlob = container.GetBlockBlobReference(blobName);
            // blockBlob.UploadFromByteArray()
            try
            {
                blockBlob.UploadFromStream(stream);
                return blockBlob;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public void DeleteBlob(string blobName, string containerName)
        {

            CloudBlobClient blobClient = _storageAccount.CreateCloudBlobClient();
            CloudBlobContainer container = blobClient.GetContainerReference(containerName);
            CloudBlockBlob blockBlob = container.GetBlockBlobReference(blobName);
            blockBlob.Delete();
        }

        public CloudBlockBlob DownloadBlob(string blobName, string containerName)
        {
            CloudBlobClient blobClient = _storageAccount.CreateCloudBlobClient();
            CloudBlobContainer container = blobClient.GetContainerReference(containerName);
            CloudBlockBlob blockBlob = container.GetBlockBlobReference(blobName);
            // blockBlob.DownloadToStream(Response.OutputStream);
            return blockBlob;
        }
    }
}