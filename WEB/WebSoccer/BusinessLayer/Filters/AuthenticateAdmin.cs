﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Filters;
using System.Web.Routing;

namespace WebSoccer.BusinessLayer.Filters
{
    public class AuthenticateAdmin : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext context)
        {
                if (context.HttpContext.Session != null && context.HttpContext.Session.Contents["admin"] == null)
                {

                    if (context.RouteData.Values["action"].ToString() != "Login" && context.RouteData.Values["action"].ToString() != "MenuPartial" && context.RouteData.Values["controller"].ToString() != "Mobile")
                    {
                        RouteValueDictionary redirectTargetDictionary = new RouteValueDictionary
                    {
                        {"action", "Login"},
                        {"controller", "Account"},
                        {"id", ""}
                    };
                        context.Result = new RedirectToRouteResult(redirectTargetDictionary);
                    }
                }

                base.OnActionExecuting(context);
    }
    }
}