﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebSoccer.Models;

namespace WebSoccer.Controllers
{
    public class AccountController : Controller
    {
        //
        // GET: /Account/
        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(Account account)
        {
            if (account.Username == "admin" && account.Password == "admin")
            {
                Session["admin"] = account;
                return RedirectToAction("Index","News");
            }
            return View(account);
        }

        [HttpPost]
        public ActionResult Logout()
        {
            Session.Remove("admin");
            return RedirectToAction("Login");
        }
	}
}