﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebSoccer.BusinessLayer.Filters;
using WebSoccer.Models;
using WebSoccer.Repository;

namespace WebSoccer.Controllers
{
    [AuthenticateAdmin]
    public class CategoryController : Controller
    {
        readonly AllRepository _repository = new AllRepository();


        // GET: /Category/
        public ActionResult Index()
        {
            List<Category> categoryList = _repository.GetCategories();
            return View(categoryList);
        }

        public ActionResult MenuPartial()
        {
            List<Category> categoryList = _repository.GetCategories();
            ViewBag.menu = categoryList;
            return PartialView("_MenuPartial");
        }

        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Create(Category category)
        {
            if (_repository.AddCategory(category) > 0)
            {
                return RedirectToAction("Index");
            }
            return View(category);
        }

        public ActionResult Edit(int id)
        {
            if (id > 0)
            {
                Category category = _repository.GetCategory(id);
                if (category != null)
                {
                    return View(category);
                }
                else
                {
                    return HttpNotFound();
                }
            }
            return new HttpNotFoundResult();
        }

        [HttpPost]
        public ActionResult Edit(Category category)
        {
            if (_repository.UpdateCategory(category) > 0)
            {
                return RedirectToAction("Index");
            }
            return View(category);
        }

        public ActionResult Delete(int id)
        {
            if (_repository.DeleteCategory(id) > 0)
            {
                return RedirectToAction("Index");
            }
            return RedirectToAction("Index");
        }
    }
}