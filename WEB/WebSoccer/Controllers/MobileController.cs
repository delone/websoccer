﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Microsoft.Ajax.Utilities;
using Newtonsoft.Json;
using WebSoccer.Models;
using WebSoccer.Repository;

namespace WebSoccer.Controllers
{

    public class MobileController : Controller
    {
        AllRepository repository = new AllRepository();
        // GET: Mobile
        public Int32 Index()
        {
            return 12; 
        }

        [HttpPost]
        public JsonResult GetAllNews(Int32 CategoryId = -1, Int32 Page = 1)
        {
            WebSoccer.Models.Object obj = new WebSoccer.Models.Object();
            obj.newsList = repository.GetAllNews(CategoryId, Page);
            return Json(obj);
        }
      
        [HttpPost]
        public JsonResult GetCategories()
        {
            WebSoccer.Models.Object obj = new WebSoccer.Models.Object();
            obj.categoryList = repository.GetCategories();
            return Json(obj);
        }
        
        [HttpPost]
        public JsonResult GetNews(Int32 Id = 0)
        {
            News news = repository.GetNews(Id);
            return Json( news);
        }

        public JsonResult AddNews()
        {
            return Json(repository.AddNews(new News()
            {
                Title = "dsd1",
                Content = "sdsda",
                CategoryId = 1,
                PublishDate = DateTime.Now,
                PhotoUrl = "nophoto"
            }));
        }

        public JsonResult UpdateNews()
        {
            return Json(repository.UpdateNews(new News()
            {
                Id = 1,
                Title = "dsd2",
                Content = "sdsda2",
                CategoryId = 1,
                PublishDate = DateTime.Now.AddDays(100),
                PhotoUrl = "nophoto2"
            }));
        }

        public JsonResult DeleteNews(Int32 Id = 0)
        {
            return Json(repository.DeleteNews(Id));
        }

        public JsonResult GetCategory(Int32 Id = 0)
        {
            Category categoty = repository.GetCategory(Id);
            return Json(categoty);
        }

        public JsonResult AddCategory()
        {
            return Json(repository.AddCategory(new Category()
            {
                Name = "11"
            }));
        }

        public JsonResult UpdateCategory()
        {
            return Json(repository.UpdateCategory(new Category()
            {
                Id = 1,
                Name = "123"
            }));
        }

        public JsonResult DeleteCategory(Int32 Id = 0)
        {
            return Json(repository.DeleteCategory(Id));
        }

        [HttpPost]
        public JsonResult AddtoFavorites(Favorites favorite)
        {
            Int32 result = 0;
            switch (favorite.SetAction)
            {
                case 1: result = repository.AddFavorite(favorite);
                    break;
                case 2: 
                    result = repository.DeleteFavorite(favorite);
                    break;
                case 3:
                    result = repository.GetFavorite(favorite);
                    break;
            }
            return Json(result);
        }

        [HttpPost]
        public JsonResult GetFavoriteList(Favorites favorite)
        {
            return Json(repository.GetFavoriteList(favorite));
        }

        [HttpPost]
        public JsonResult Test()
        {
            var request = WebRequest.Create("https://onesignal.com/api/v1/notifications") as HttpWebRequest;

            request.KeepAlive = true;
            request.Method = "POST";
            request.ContentType = "application/json; charset=utf-8";

            request.Headers.Add("authorization", "Basic ZjMzMGNhZmMtYTdkMi00MjRhLWE0ZjktMDA1NGJhMWMzYjcy");

           


            byte[] byteArray = Encoding.UTF8.GetBytes("{"
                                                    + "\"app_id\": \"321cb2e9-a5f6-4f6c-9bd8-0e8d22e65236\","
                                                    + "\"contents\": {\"en\": \"English Message\"},"
                                                    + "\"data\": {\"news_id\": \"1\"},"
                                                    + "\"included_segments\": [\"All\"]}");

            string responseContent = null;

            try
            {
                using (var writer = request.GetRequestStream())
                {
                    writer.Write(byteArray, 0, byteArray.Length);
                }

                using (var response = request.GetResponse() as HttpWebResponse)
                {
                    using (var reader = new StreamReader(response.GetResponseStream()))
                    {
                        responseContent = reader.ReadToEnd();
                    }
                }
            }
            catch (WebException ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                System.Diagnostics.Debug.WriteLine(new StreamReader(ex.Response.GetResponseStream()).ReadToEnd());
            }

            return Json(responseContent);

        }
    }
}