﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Web.Mvc;
using WebSoccer.BusinessLayer.FileUpload;
using WebSoccer.BusinessLayer.Filters;
using WebSoccer.Models;
using WebSoccer.Repository;

namespace WebSoccer.Controllers
{
    [AuthenticateAdmin]
    public class NewsController : Controller
    {
        readonly AllRepository _repository = new AllRepository();

        readonly BlobUtility _utility = new BlobUtility();
        
        // GET: /News/
        public ActionResult Index(Int32? categoryId = -1,Int32? page = 1)
        {
            ViewBag.CurrentPage = page;
            ViewBag.TotalRows = _repository.GetTotalRowsofNews(categoryId);
            if (categoryId > -2)
            {
                List<News> newsList = _repository.GetAllNews(categoryId, page);
                if (newsList == null)
                {
                    return HttpNotFound();
                }
                return View(newsList);
            }
            return RedirectToAction("Index","Category");
        }

        public ActionResult Details(int id)
        {
            if (id > 0)
            {
                News news = _repository.GetNews(id);
                if (news == null)
                {
                    return HttpNotFound();
                }
                return View(news);
            }
            return View();
        }
        
        public ActionResult Create()
        {
            SetDropdownViewBag();
            return View();
        }
        
        [HttpPost]
        public ActionResult Create(News news)
        {
            SetDropdownViewBagSelected(news);
            
            if (!ModelState.IsValid)
            {
               return View(news);
            }
            // Extract file from Posted Students object
            string ContainerName = "Images"; //hardcoded container name. 
            //string fileName = Path.GetFileName(news.Photo.FileName);
            string fileName = new Random().Next() + Path.GetExtension(news.Photo.FileName);
            Stream imageStream = news.Photo.InputStream;
            var result = _utility.UploadBlob(fileName, ContainerName, imageStream);
            news.PhotoUrl = result.Uri.ToString();

            var success = _repository.AddNews(news);
            if (success > 0)
            {
                PushNotification(success,news.Title);
                return RedirectToAction("Index","News",new {id = news.CategoryId});
            }
            return View(news);
        }

        public ActionResult Edit(int id)
        {
            if (id > 0)
            {
                News news = _repository.GetNews(id);
                if (news != null)
                {
                    SetDropdownViewBagSelected(news);
                    return View(news);
                }
                else
                {
                    return HttpNotFound();
                }
            }
            return new HttpNotFoundResult();
        }

        [HttpPost]
        public ActionResult Edit(News news)
        {
            
         SetDropdownViewBagSelected(news);

            if (!ModelState.IsValid)
            {
                return View(news);
            }
            else
            {
                // Extract file from Posted Students object
                if (news.Photo != null)
                {
                    string ContainerName = "Images"; //hardcoded container name. 
                    //string fileName = Path.GetFileName(news.Photo.FileName);
                    string fileName = new Random().Next() + Path.GetExtension(news.Photo.FileName);
                    Stream imageStream = news.Photo.InputStream;
                    var result = _utility.UploadBlob(fileName, ContainerName, imageStream);
                    news.PhotoUrl = result.Uri.ToString();
                }
                else
                {
                    news.PhotoUrl = "";
                }
                var success = _repository.UpdateNews(news);
                if (success > 0)
                {      
                    return RedirectToAction("Index", new { id = news.CategoryId });
                }
                return View(news);
            }
        }

        public ActionResult Delete(int id)
        {
            if (_repository.DeleteNews(id) > 0)
            {
                return RedirectToAction("Index",new {id = 1});
            }
            return RedirectToAction("Index", new {id = 1});
        }

        private void SetDropdownViewBag()
        {
            var categoryList = _repository.GetCategories();
            List<SelectListItem> categories = new List<SelectListItem>();
            foreach (Category category in categoryList)
            {
                categories.Add(new SelectListItem { Text = category.Name, Value = category.Id.ToString() });
            }
            ViewBag.Categories = categories;
        }




        private void SetDropdownViewBagSelected(News news)
        {
            List<Category> categoryList;
            categoryList = _repository.GetCategories();
            List<SelectListItem> categories = new List<SelectListItem>();
            foreach (Category category in categoryList)
            {
                categories.Add(new SelectListItem { Text = category.Name, Value = category.Id.ToString() });
            }
            ViewBag.Categories = new SelectList(categories,"Value","Text",news.CategoryId);
        }

        private void PushNotification(Int32 newsId,String title)
        {
            var request = WebRequest.Create("https://onesignal.com/api/v1/notifications") as HttpWebRequest;

            if (request != null)
            {
                request.KeepAlive = true;
                request.Method = "POST";
                request.ContentType = "application/json; charset=utf-8";

                request.Headers.Add("authorization", "Basic ZjMzMGNhZmMtYTdkMi00MjRhLWE0ZjktMDA1NGJhMWMzYjcy");




                //byte[] byteArray = Encoding.UTF8.GetBytes("{"
                //                                        + "\"app_id\": \"321cb2e9-a5f6-4f6c-9bd8-0e8d22e65236\","
                //                                        + "\"contents\": {\"en\": \"English Message\"},"
                //                                        + "\"data\": {\"news_id\": \"1\"},"
                //                                        + "\"included_segments\": [\"All\"]}");

                byte[] byteArray = Encoding.UTF8.GetBytes("{"
                                                          + "\"app_id\": \"321cb2e9-a5f6-4f6c-9bd8-0e8d22e65236\","
                                                          + "\"contents\": {\"en\":" + "\"" +  title + "\"" + "},"
                                                          + "\"data\": {\"news_id\":" +  "\"" + newsId.ToString() + "\"" + "},"
                                                          + "\"included_segments\": [\"All\"]}");

                try
                {
                    using (var writer = request.GetRequestStream())
                    {
                        writer.Write(byteArray, 0, byteArray.Length);
                    }

                    using (var response = request.GetResponse() as HttpWebResponse)
                    {
                        if (response != null)
                            using (var reader = new StreamReader(response.GetResponseStream()))
                            {
                                reader.ReadToEnd();
                            }
                    }
                }
                catch (WebException ex)
                {
                    System.Diagnostics.Debug.WriteLine(ex.Message);
                    System.Diagnostics.Debug.WriteLine(new StreamReader(stream: ex.Response.GetResponseStream()).ReadToEnd());
                }
            }
        }
    }
}