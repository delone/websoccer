﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebSoccer.Models
{
    public class Category
    {
        public Int32 Id { get; set; }

        public String Name { get; set; }
    }
}