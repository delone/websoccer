﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebSoccer.Models
{
    public class Favorites
    {
        public Int32 Id { get; set; }

        public String AppId { get; set; }

        public Int32 SetAction { get; set; }

        public Int32 CategoryId { get; set; }
    }
}