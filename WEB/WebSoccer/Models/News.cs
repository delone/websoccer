﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace WebSoccer.Models
{
    public class News
    {
        public Int32 Id { get; set; }

        public String Title { get; set; }

        public String Content { get; set; }

        public DateTime? PublishDate { get; set; }

        public String PhotoUrl { get; set; }

        public HttpPostedFileWrapper Photo { get; set; }

        public Int32 CategoryId { get; set; }

        public Int32? ViewCount { get; set; }

    }
}