﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using WebSoccer.Models;

namespace WebSoccer.Repository
{
   
    public class AllRepository
    {

        String ConnectionString = "Server=tcp:mobsoccer.database.windows.net,1433;Initial Catalog=MobSoccer;Persist Security Info=False;User ID=mobsoccer;Password=mob.soccer1;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;";

        public List<Category> GetCategories()
        {
            List<Category> categories = new List<Category>();
            try
            {
                var connection = new SqlConnection(ConnectionString);
                connection.Open();
                var command = new SqlCommand
                {
                    Connection = connection,
                    CommandType = CommandType.Text,
                    CommandText = @"SELECT * from Category order by Id desc ;"
                };

                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    categories.Add(new Category()
                    {
                        Id = reader.GetInt32(reader.GetOrdinal("Id")),
                        Name = reader["Name"] as string
                    });
                }
                connection.Close();
            }
            catch (Exception) {
                categories = new List<Category>();
            }

            return categories;
        }
        
        public List<News> GetAllNews(Int32? categoryId = -1,Int32? page = 1)
        {
            List<News> newsList = new List<News>();
            try
            {
                var connection = new SqlConnection(ConnectionString);
                connection.Open();

                var command = new SqlCommand
                {
                    Connection = connection,
                    CommandType = CommandType.Text,                    

                CommandText = @"select * from(
                    SELECT ROW_NUMBER() OVER(ORDER BY Id DESC) AS Row, * from News where
@P_CategoryId = -1 or @P_CategoryId  = CategoryId)c where Row > ((@P_Page - 1) * 10) and Row <= ( @P_Page * 10);"
                };

                SqlParameter pCategoryId = new SqlParameter("@P_CategoryId", SqlDbType.Int) {Value = categoryId};
                command.Parameters.Add(pCategoryId);

                SqlParameter pPage = new SqlParameter("@P_Page", SqlDbType.Int) {Value = page};
                command.Parameters.Add(pPage);

            SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    newsList.Add(new News()
                    {
                        Id = reader.GetInt32(reader.GetOrdinal("Id")),
                        Title = reader["Title"] as string,
                        Content = reader["Content"] as string,
                         PhotoUrl = reader["PhotoUrl"] as string,
                        PublishDate = reader["PublishDate"] as DateTime? ?? default(DateTime),
                    });
                }
                connection.Close();
            }
            catch (Exception)
            {
                newsList = new List<News>();
            }

            return newsList;
        }

        public Int32 GetTotalRowsofNews(Int32? categoryId = -1)
        {
           Int32 result = 0;
            try
            {
                var connection = new SqlConnection(ConnectionString);
                connection.Open();

                var command = new SqlCommand
                {
                    Connection = connection,
                    CommandType = CommandType.Text,

                    CommandText = @"select count(id) from News where
                     @P_CategoryId = -1 or @P_CategoryId  = CategoryId;"
                };

                SqlParameter pCategoryId = new SqlParameter("@P_CategoryId", SqlDbType.Int) {Value = categoryId};
                command.Parameters.Add(pCategoryId);


                result = (Int32)command.ExecuteScalar();

                
                connection.Close();
            }
            catch (Exception)
            {
                result = 0;
            }

            return result;
        }


        public News GetNews(Int32 id = 0)
        {
            News news = new News();
            try
            {
                var connection = new SqlConnection(ConnectionString);
                connection.Open();

                var command = new SqlCommand
                {
                    Connection = connection,
                    CommandType = CommandType.Text,
                    CommandText = @"SELECT * from News where @P_Id  = Id ;"
                };

                SqlParameter pId = new SqlParameter("@P_Id", SqlDbType.Int);
                pId.Value = id;
                command.Parameters.Add(pId);


                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    news = new News()
                    {
                        Id = reader.GetInt32(reader.GetOrdinal("Id")),
                        Title = reader["Title"] as string,
                        Content = reader["Content"] as string,
                        PhotoUrl = reader["PhotoUrl"] as string,
                        PublishDate = reader["PublishDate"] as DateTime? ?? default(DateTime),
                        CategoryId = reader.GetInt32(reader.GetOrdinal("CategoryId"))
                    };
                }
                connection.Close();
            }
            catch (Exception)
            {
                news = new News();
            }

            return news;
        }
                
        public Int32 AddNews(News news)
        {
            Int32 result;
            try
            {
                var connection = new SqlConnection(ConnectionString);
                connection.Open();

                var command = new SqlCommand
                {
                    Connection = connection,
                    CommandType = CommandType.Text,
                    CommandText = @"Insert Into News(Title,Content,CategoryId,PhotoUrl,PublishDate)OUTPUT Inserted.ID
                                           Values(@P_Title,@P_Content,@P_CategoryId,@P_PhotoUrl,@P_PublishDate);"
                };

                AddParameterofNews(news, command);

                 result = (Int32)command.ExecuteScalar();
                 connection.Close();
            }
            catch (Exception)
            {
                result = -1;
            }
            return result;
        }

        public Int32 UpdateNews(News news)
        {
            Int32 result;
            try
            {
                var connection = new SqlConnection(ConnectionString);
                connection.Open();

                var command = new SqlCommand
                {
                    Connection = connection,
                    CommandType = CommandType.Text,
                    CommandText = @"Update News set Title = @P_Title ,
                                                    Content = @P_Content,
                                                    CategoryId = @P_CategoryId,
                                                    PhotoUrl = ISNULL(@P_PhotoUrl,News.PhotoUrl) ,
                                                    PublishDate = @P_PublishDate
                                                    where Id = @P_Id;"
                };

                SqlParameter pId = new SqlParameter("@P_Id", SqlDbType.Int);
                pId.Value = news.Id;
                command.Parameters.Add(pId);

                AddParameterofNews(news, command);

                result = command.ExecuteNonQuery();
                connection.Close();
            }
            catch (Exception)
            {
                result = -1;
            }
            return result;
        }

        public Int32 DeleteNews(Int32 id = 0 )
        {
            Int32 result;
            try
            {
                var connection = new SqlConnection(ConnectionString);
                connection.Open();

                var command = new SqlCommand
                {
                    Connection = connection,
                    CommandType = CommandType.Text,
                    CommandText = @"Delete from News where Id = @P_Id;"
                };

                SqlParameter pId = new SqlParameter("@P_Id", SqlDbType.Int);
                pId.Value = id;
                command.Parameters.Add(pId); 
                result = command.ExecuteNonQuery();
                connection.Close();
            }
            catch (Exception)
            {
                result = -1;
            }
            return result;
        }

        public Category GetCategory(Int32 id = 0)
        {
            Category category = new Category();
            try
            {
                var connection = new SqlConnection(ConnectionString);
                connection.Open();

                var command = new SqlCommand
                {
                    Connection = connection,
                    CommandType = CommandType.Text,
                    CommandText = @"SELECT * from Category where @P_Id = Id ;"
                };

                SqlParameter pId = new SqlParameter("@P_Id", SqlDbType.Int) {Value = id};
                command.Parameters.Add(pId);


                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    category = new Category()
                    {
                        Id = reader.GetInt32(reader.GetOrdinal("Id")),
                        Name = reader["Name"] as string
                    };
                }
                connection.Close();
            }
            catch (Exception)
            {
                category = new Category();
            }

            return category ;
        }

        public Int32 AddCategory(Category category)
        {
            Int32 result;
            try
            {
                var connection = new SqlConnection(ConnectionString);
                connection.Open();

                var command = new SqlCommand
                {
                    Connection = connection,
                    CommandType = CommandType.Text,
                    CommandText = @"Insert Into Category(Name) Values(@P_Name);"
                };

                AddParameterofCategories(category, command);

                result = command.ExecuteNonQuery();
                connection.Close();
            }
            catch (Exception)
            {
                result = -1;
            }
            return result;
        }

        public Int32 UpdateCategory(Category category)
        {
            Int32 result;
            try
            {
                var connection = new SqlConnection(ConnectionString);
                connection.Open();

                var command = new SqlCommand
                {
                    Connection = connection,
                    CommandType = CommandType.Text,
                    CommandText = @"Update Category set Name = @P_Name where Id = @P_Id;"
                };

                SqlParameter pId = new SqlParameter("@P_Id", SqlDbType.Int) {Value = category.Id};
                command.Parameters.Add(pId);

                AddParameterofCategories(category, command);

                result = command.ExecuteNonQuery();
                connection.Close();
            }
            catch (Exception)
            {
                result = -1;
            }
            return result;
        }

        public Int32 DeleteCategory(Int32 id = 0)
        {
            Int32 result;
            try
            {
                var connection = new SqlConnection(ConnectionString);
                connection.Open();

                var command = new SqlCommand
                {
                    Connection = connection,
                    CommandType = CommandType.Text,
                    CommandText = @"Delete from Category where Id = @P_Id;"
                };

                SqlParameter pId = new SqlParameter("@P_Id", SqlDbType.Int);
                pId.Value = id;
                command.Parameters.Add(pId);
                result = command.ExecuteNonQuery();
                connection.Close();
            }
            catch (Exception)
            {
                result = -1;
            }
            return result;
        }

        

        public Int32 AddFavorite(Favorites favorite)
        {
            Int32 result = -1;
            try
            {
                if (GetFavorite(favorite) < 1)
                {
                    var connection = new SqlConnection(ConnectionString);
                    connection.Open();

                    var command = new SqlCommand
                    {
                        Connection = connection,
                        CommandType = CommandType.Text,
                        CommandText = @"Insert Into Favorites(AppId,CategoryId)
                                           Values(@P_AppId,@P_CategoryId);"
                    };

                    AddParameterofFavorites(favorite, command);

                    result = command.ExecuteNonQuery();
                    connection.Close();
                }
            }
            catch (Exception)
            {
                result = -1;
            }
            return result;
        }

        public Int32 UpdateFavorite(Favorites favorite)
        {
            Int32 result;
            try
            {
                var connection = new SqlConnection(ConnectionString);
                connection.Open();

                var command = new SqlCommand
                {
                    Connection = connection,
                    CommandType = CommandType.Text,
                    CommandText = @"Update Favorites set AppId = @P_AppId ,                                                   
                                                    CategoryId = @P_CategoryId,
                                                    where Id = @P_Id;"
                };

                SqlParameter pId = new SqlParameter("@P_Id", SqlDbType.Int);
                pId.Value = favorite.Id;
                command.Parameters.Add(pId);

                AddParameterofFavorites(favorite, command);

                result = command.ExecuteNonQuery();
            }
            catch (Exception)
            {
                result = -1;
            }
            return result;
        }

        public Int32 DeleteFavorite(Favorites favorite)
        {
            Int32 result;
            try
            {
                var connection = new SqlConnection(ConnectionString);
                connection.Open();

                var command = new SqlCommand
                {
                    Connection = connection,
                    CommandType = CommandType.Text,
                    CommandText = @"Delete from Favorites where @P_AppId  = AppId and @P_CategoryId = CategoryId;"
                };

                AddParameterofFavorites(favorite,command);
                result = command.ExecuteNonQuery();
            }
            catch (Exception)
            {
                result = -1;
            }
            return result;
        }

        public Int32 GetFavorite(Favorites favorite)
        {
            Int32 result = -1;
            try
            {
                var connection = new SqlConnection(ConnectionString);
                connection.Open();

                var command = new SqlCommand
                {
                    Connection = connection,
                    CommandType = CommandType.Text,
                    CommandText = @"SELECT * from Favorites where @P_AppId  = AppId and @P_CategoryId = CategoryId;"
                };

                AddParameterofFavorites(favorite,command);
                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    result = 1;
                    break;
                }
            }
            catch (Exception)
            {
                result = -1;
            }

            return result;
        }

        public List<Category> GetFavoriteList(Favorites favorite)
        {
            List<Category> list = new List<Category>();
            try
            {
                var connection = new SqlConnection(ConnectionString);
                connection.Open();

                var command = new SqlCommand
                {
                    Connection = connection,
                    CommandType = CommandType.Text,
                    CommandText = @"SELECT distinct CategoryId from Favorites where @P_AppId  = AppId;"
                };

                AddParameterofFavorites(favorite, command);
                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    list.Add(new Category()
                    {
                        Id = reader.GetInt32(reader.GetOrdinal("CategoryId"))
                    });
                }
            }
            catch (Exception)
            {
                list = new List<Category>();
            }

            return list;
        }

        private void AddParameterofNews(News news, SqlCommand command)
        {
            SqlParameter pTitle = new SqlParameter("@P_Title", SqlDbType.NVarChar) {Value = news.Title};
            command.Parameters.Add(pTitle);

            SqlParameter pContent = new SqlParameter("@P_Content", SqlDbType.NVarChar) {Value = news.Content};
            command.Parameters.Add(pContent);

            SqlParameter pCategoryId = new SqlParameter("@P_CategoryId", SqlDbType.Int) {Value = news.CategoryId};
            command.Parameters.Add(pCategoryId);

            SqlParameter pPhotoUrl = new SqlParameter("@P_PhotoUrl", SqlDbType.NVarChar) {Value = news.PhotoUrl};
            command.Parameters.Add(pPhotoUrl);

            SqlParameter pPublishDate = new SqlParameter("@P_PublishDate", SqlDbType.DateTime) {Value = DateTime.Now};
            command.Parameters.Add(pPublishDate);
        }

        private void AddParameterofFavorites(Favorites favorite, SqlCommand command)
        {
            SqlParameter pAppId = new SqlParameter("@P_AppId", SqlDbType.NVarChar) {Value = favorite.AppId};
            command.Parameters.Add(pAppId);

            SqlParameter pCategoryId = new SqlParameter("@P_CategoryId", SqlDbType.Int) {Value = favorite.CategoryId};
            command.Parameters.Add(pCategoryId);

           }
        
        private void AddParameterofCategories(Category category, SqlCommand command)
        {
            SqlParameter pName = new SqlParameter("@P_Name", SqlDbType.NVarChar) {Value = category.Name};
            command.Parameters.Add(pName); 
        }
    }
}