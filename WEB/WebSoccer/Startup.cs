﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(WebSoccer.Startup))]
namespace WebSoccer
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
